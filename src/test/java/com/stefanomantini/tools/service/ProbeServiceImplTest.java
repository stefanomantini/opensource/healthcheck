package com.stefanomantini.tools.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import com.stefanomantini.tools.common.model.EndpointConfigList;
import com.stefanomantini.tools.common.model.Probe;
import com.stefanomantini.tools.service.contract.ProbeService;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

public class ProbeServiceImplTest extends AbstractServiceTest {

  @Autowired EndpointConfigList endpointConfigList;

  @Autowired ProbeService probeService;

  @Test
  @Ignore
  public void getProbes_returns_ok() {
    for (int i = 0; i < this.endpointConfigList.getProbes().size(); i++) {
      final Stream<Probe> probes = this.probeService.getProbes();
      System.out.println(
          "S: "
              + i
              + " "
              + this.probeService.buildEndpoint(this.endpointConfigList.getProbes().get(i))
              + " -- "
              + probes.collect(Collectors.toList()).get(i).getEndpoint()
              + " :E");
      assertEquals(
          this.endpointConfigList.getProbes().get(i).getEndpoint(),
          probes.collect(Collectors.toList()).get(i).getEndpoint());
    }
  }

  @Test
  public void buildEndpoint_returns_ok() {
    String expected = "http://localhost:8080/health";
    String result = this.probeService.buildEndpoint("http", "localhost", 8080, "health");
    assertEquals(expected, result);
  }

  @Test
  public void getClientResponse_returns_ok() {
    Mono<ClientResponse> cr = this.probeService.getClientResponse("http://localhost:8888/health");
    System.out.println(cr);
  }

  @Test
  public void aggregateHealthChecks_returns_ok() {
    // GIVEN
    final WebClient mock = Mockito.mock(WebClient.class);
    final WebClient.RequestHeadersUriSpec uriSpecMock =
        Mockito.mock(WebClient.RequestHeadersUriSpec.class);

    when(mock.get()).thenReturn(uriSpecMock);

    // THEN
    assertEquals(
        this.endpointConfigList.getProbes().size(),
        this.probeService.aggregateHealthChecks().size());
  }
}
