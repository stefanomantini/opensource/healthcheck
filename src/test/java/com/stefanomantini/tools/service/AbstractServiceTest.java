package com.stefanomantini.tools.service;

import com.stefanomantini.tools.HealthCheckApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = HealthCheckApplication.class)
public class AbstractServiceTest {
  protected final Logger log = LoggerFactory.getLogger(AbstractServiceTest.class);

  @Test
  public void contextLoads() {}
}
