package com.stefanomantini.tools.web;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import java.io.IOException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AbstractIntegrationTest {

  protected final Logger log = LoggerFactory.getLogger(this.getClass());

  protected final HttpHeaders defaultHeaders = new HttpHeaders();

  protected RestTemplate rt;

  protected String wiremockUri;

  protected String baseUri;

  private final int wiremockPort = 7777;

  @LocalServerPort protected int localServerPort;

  @Autowired ResourcePatternResolver resourcePatternResolver;

  @Rule public WireMockRule wireMock = new WireMockRule(wireMockConfig().port(this.wiremockPort));

  @Before
  public void setup() throws IOException {
    this.rt = new RestTemplate();
    this.wiremockUri = "http://localhost:" + this.wiremockPort;
    this.baseUri = "http://localhost:" + this.localServerPort;
  }

  @Test
  public void contextLoads() {}
}
