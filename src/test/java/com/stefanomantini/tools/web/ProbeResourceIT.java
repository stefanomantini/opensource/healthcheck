package com.stefanomantini.tools.web;

import static org.junit.Assert.assertTrue;

import com.stefanomantini.tools.common.enumeration.HealthStatus;
import com.stefanomantini.tools.common.model.Probe;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProbeResourceIT extends AbstractIntegrationTest {

  @Test
  public void testDefaultMockConfig() throws Exception {
    ParameterizedTypeReference<List<Probe>> responseType =
        new ParameterizedTypeReference<List<Probe>>() {};
    ResponseEntity<List<Probe>> resp =
        this.rt.exchange(
            this.baseUri + "/api/probes", HttpMethod.GET, RequestEntity.EMPTY, responseType);
    resp.getBody().stream().forEach(e -> assertTrue(e.getStatus().equals(HealthStatus.UP)));
  }
}
