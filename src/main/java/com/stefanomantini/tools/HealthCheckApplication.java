package com.stefanomantini.tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HealthCheckApplication {

  private static Logger logger = LoggerFactory.getLogger(HealthCheckApplication.class);

  /**
   * @param args
   * @throws Exception
   */
  public static void main(final String[] args) throws Exception {
    SpringApplication.run(HealthCheckApplication.class, args);
  }
}
