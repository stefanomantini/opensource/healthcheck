package com.stefanomantini.tools.common.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Validated
public class EndpointConfig {

  private Long id;

  @NotEmpty private String host;

  @Min(0)
  @Max(99999)
  @NotEmpty
  private Integer port;

  private String endpoint;

  @Min(100)
  @Max(599)
  @NotEmpty
  private Integer successCode;
}
