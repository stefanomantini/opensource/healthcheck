package com.stefanomantini.tools.common.model;

import com.stefanomantini.tools.common.enumeration.HealthStatus;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Probe {
  @NotEmpty private String endpoint;
  @NotEmpty private HttpStatus responseStatus;
  @NotEmpty private Long responseTime;
  @NotEmpty private HealthStatus status;
  @NotEmpty private HttpStatus expectedResponseStatus;
}
