package com.stefanomantini.tools.common.enumeration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString(callSuper = true)
public enum Protocol {
  HTTP("http://"),
  HTTPS("https://");

  private final String value;

  private static final Map<String, Protocol> NAME_MAP =
      Arrays.stream(values()).collect(Collectors.toMap(v -> v.value, v -> v));

  @JsonCreator
  public static Protocol forValue(String value) {
    return Optional.ofNullable(NAME_MAP.get(value)).orElseThrow(IllegalArgumentException::new);
  }

  @JsonValue
  public String getValue() {
    return value;
  }
}
