package com.stefanomantini.tools.common.enumeration;

public enum HealthStatus {
  UP,
  DOWN;
}
