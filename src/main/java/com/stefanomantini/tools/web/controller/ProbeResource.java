package com.stefanomantini.tools.web.controller;

import com.stefanomantini.tools.common.model.Probe;
import com.stefanomantini.tools.service.contract.ProbeService;
import com.stefanomantini.tools.web.contract.ProbeResourceContract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@CrossOrigin
@RestController
@RequestMapping(path = {"/api/probes"})
public class ProbeResource implements ProbeResourceContract {

  @Autowired ProbeService probeService;

  @Override
  @GetMapping
  public Flux<Probe> getProbes() {
    return Flux.fromStream((probeService.getProbes()));
  }
}
