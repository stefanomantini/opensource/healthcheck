FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY build/libs/*.jar /app.jar
RUN echo $BITBUCKET_BUILD_NUMBER > /$BITBUCKET_BUILD_NUMBER.version
ENV CONFIG_SERVER_URL http://HOSTFILE_ENTRY:9999
ENV CONFIG_SERVER_USERNAME admin
ENV CONFIG_SERVER_PASSWORD ${CONFIG_SERVER_PASSWORD}
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]